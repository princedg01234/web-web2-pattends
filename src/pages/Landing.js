import React from "react";
import Header from "../components/Navbar/Header";
import Sidebar from "../components/Navbar/Sidebar";

import nino from "../assets/images/nino.png";

const Landing = () => {
  return (
    <>
      <Header />
      <Sidebar />
      <section id="content">
        <div>
          {/* Main */}
          <main>
            <div className="head-title">
              <div className="left">
                <h1>Dashboard</h1>
                <ul className="breadcrumb">
                  <li>
                    <span>Dashboard /</span>
                  </li>
                  <li>
                    <i className="ri ri-chevron-line" />
                  </li>
                  <li>
                    <a className="active" href="#">
                      Home
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <ul className="box-info">
              <li>
                <i className="ri ri-map-pin-user-line" />
                <span className="text">
                  <h3>3</h3>
                  <p>Students</p>
                </span>
              </li>
              <li>
                <i className="ri ri-chat-check-line" />
                <span className="text">
                  <h3>2</h3>
                  <p>Attendance</p>
                </span>
              </li>
              <li>
                <i className="ri ri-book-open-line" />
                <span className="text">
                  <h3>1</h3>
                  <p>Classes</p>
                </span>
              </li>
            </ul>
          </main>
          {/* Main */}
        </div>
      </section>
    </>
  );
};

export default Landing;
