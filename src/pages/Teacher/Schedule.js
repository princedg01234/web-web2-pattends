import Header from "../../components/Navbar/Header";
import Sidebar from "../../components/Navbar/Sidebar";

import nino from "../../assets/images/nino.png";

const Schedule = () => {
  return (
    <>
      <Header />
      <Sidebar />
      <section id="content">
        <div>
          {/* Main */}
          <main>
            <div className="head-title">
              <div className="left">
                <h1>Schedule</h1>
                <ul className="breadcrumb">
                  <li>
                    <span>Schedule /</span>
                  </li>
                  <li>
                    <i className="ri ri-chevron-line" />
                  </li>
                  <li>
                    <a className="active" href="#">
                      Home
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <ul className="box-info">
              <li>
                <i className="ri ri-map-pin-user-line" />
                <span className="text">
                  <h3>3</h3>
                  <p>Students</p>
                </span>
              </li>
              <li>
                <i className="ri ri-chat-check-line" />
                <span className="text">
                  <h3>2</h3>
                  <p>Attendance</p>
                </span>
              </li>
              <li>
                <i className="ri ri-book-open-line" />
                <span className="text">
                  <h3>1</h3>
                  <p>Classes</p>
                </span>
              </li>
            </ul>
            <div className="table-data">
              <div className="order">
                <div className="head">
                  <h3>Schedule</h3>
                  <i className="bx bx-search" />
                  <i className="bx bx-filter" />
                </div>
                <table>
                  <thead>
                    <tr>
                      <th>Subject Code</th>
                      <th>Subject Name</th>
                      <th>Time</th>
                      <th>Room</th>
                      <th>Date</th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* 1 */}
                    <tr>
                      <td>ITE 307</td>
                      <td>Math</td>
                      <td>7:30 AM</td>
                      <td>PTC 302</td>
                      <td>01/01/2023</td>
                    </tr>
                    {/* 2 */}
                    <tr>
                      <td>ITE 301</td>
                      <td>Science</td>
                      <td>7:30 AM</td>
                      <td>PTC 301</td>
                      <td>01/01/2023</td>
                    </tr>
                    {/* 3 */}
                    <tr>
                      <td>ITE 309</td>
                      <td>MAPEH</td>
                      <td>7:30 AM</td>
                      <td>PTC 401</td>
                      <td>01/01/2023</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </main>
          {/* Main */}
        </div>
      </section>
    </>
  );
};

export default Schedule;
