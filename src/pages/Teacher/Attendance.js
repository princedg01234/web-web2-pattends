import React from "react";
import Header from "../../components/Navbar/Header";
import Sidebar from "../../components/Navbar/Sidebar";

import nino from "../../assets/images/nino.png";

const Attendance = () => {
  return (
    <>
      <Header />
      <Sidebar />
      <section id="content">
        <div>
          {/* Main */}
          <main>
            <div className="head-title">
              <div className="left">
                <h1>Attendance</h1>
                <ul className="breadcrumb">
                  <li>
                    <span>Attendance /</span>
                  </li>
                  <li>
                    <i className="ri ri-chevron-line" />
                  </li>
                  <li>
                    <a className="active" href="#">
                      Home
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <ul className="box-info">
              <li>
                <i className="ri ri-map-pin-user-line" />
                <span className="text">
                  <h3>3</h3>
                  <p>Students</p>
                </span>
              </li>
              <li>
                <i className="ri ri-chat-check-line" />
                <span className="text">
                  <h3>2</h3>
                  <p>Attendance</p>
                </span>
              </li>
              <li>
                <i className="ri ri-book-open-line" />
                <span className="text">
                  <h3>1</h3>
                  <p>Classes</p>
                </span>
              </li>
            </ul>
            <div className="table-data">
              <div className="order">
                <div className="head">
                  <h3>Class</h3>
                  <i className="bx bx-search" />
                  <i className="bx bx-filter" />
                </div>
                <table>
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Student No.</th>
                      <th>Name</th>
                      <th>Date</th>
                      <th>Attendance</th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* 1 */}
                    <tr>
                      <td>
                        <img src={nino} />
                      </td>
                      <td>03-2223-01234</td>
                      <td>Niño Barganza</td>
                      <td>01/01/2011</td>
                      <td>
                        <span className="status completed">Completed</span>
                      </td>
                    </tr>
                    {/* 2 */}
                    <tr>
                      <td>
                        <img src={nino} />
                      </td>
                      <td>03-2223-05678</td>
                      <td>Niño Barganzo</td>
                      <td>01/01/2011</td>
                      <td>
                        <span className="status failed">Absent</span>
                      </td>
                    </tr>
                    {/* 3 */}
                    <tr>
                      <td>
                        <img src={nino} />
                      </td>
                      <td>03-2223-09101</td>
                      <td>Niño Borganza</td>
                      <td>01/01/2011</td>
                      <td>
                        <span className="status pending">Late</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </main>
          {/* Main */}
        </div>
      </section>
    </>
  );
};

export default Attendance;
