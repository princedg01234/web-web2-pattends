import React from "react";
import logo from "../../assets/images/pattends.png";
import "../Navbar/Header.css";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <>
      {/* <!-- SIDEBAR --> */}
      <section id="sidebar">
        <Link to="/" class="brand">
          <i class="bx bxs-smile"></i>
          <span className="pt-10 pr-6">
            <img
              src={logo}
              className="w-[200px] ml-[-20px] top-[-30px] fixed"
            />
          </span>
        </Link>
        <ul class="side-menu top">
          <li class="active">
            <a href="#">
              <i className="ri-dashboard-line px-2"></i>
              <span class="text">Dashboard</span>
            </a>
          </li>

          <li>
            <Link to="/schedule">
              <i className="ri-calendar-check-line px-2"></i>
              <span class="text">Class Schedule</span>
            </Link>
          </li>

          <li>
            <Link to="/attend">
              <i class="ri-checkbox-circle-line px-2"></i>
              <span class="text">Attendance</span>
            </Link>
          </li>

          <li>
            <a href="#">
              <i class="ri-eye-line px-2"></i>
              <span class="text">View Students</span>
            </a>
          </li>

          <li>
            <Link to="/login" class="logout">
              <i class="ri-logout-circle-line px-2"></i>
              <span class="text">Logout</span>
            </Link>
          </li>
        </ul>
      </section>
      {/* <!-- SIDEBAR --> */}
    </>
  );
};

export default Sidebar;
