import React from "react";

import nino from "../../assets/images/nino.png";
import "../Navbar/Header.css";

const Header = () => {
  return (
    <>
      <section id="content">
        <nav>
          <i className="bx bx-menu" />
          <form action="#">
            <div className="form-input">
              <input type="search" placeholder="Search..." />
              <button type="submit" className="search-btn">
                <i className="ri-search-line" />
              </button>
            </div>
          </form>
          <input type="checkbox" id="switch-mode" hidden />
          <label htmlFor="switch-mode" className="switch-mode" />
          <a href="#" className="notification">
            <i className="ri-bell-line" />
          </a>
          <a href="#" className="profile">
            <img src={nino} />
          </a>
        </nav>
      </section>
    </>
  );
};

export default Header;
