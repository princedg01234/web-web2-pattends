import "./App.css";
import Aos from "aos";
import { useEffect } from "react";

// import routes
import { Routes, Route } from "react-router-dom";

// import components
import Login from "./components/Login Page/Login";
import Register from "./components/Register Page/Register";
// Admin
import AdminPage from "./pages/Admin/AdminPage";

// Teacher
import Attend from "./pages/Teacher/Attendance";
import Landing from "./pages/Landing";
import Schedule from "./pages/Teacher/Schedule";

function App() {
  useEffect(() => {
    Aos.init();
  }, []);

  return (
    <>
      <div className="mx-auto">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/home" element={<Landing />} />
          {/* Admin */}
          <Route path="/admin" element={<AdminPage />} />
          <Route path="/attend" element={<Attend />} />
          {/* Teacher */}
          <Route path="/schedule" element={<Schedule />} />
        </Routes>
      </div>
    </>
  );
}

export default App;
